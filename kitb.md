# Final Report
[TOC]

* What I set out to do
* How I did it
* What I acheived of my goals
* My evaluation criteria
* Critical analysis

## My system
In order for the project to work we required a server process that could model
the state of the world and relay information between game clients and the base
station. As the link between the base station and the internet is relatively
high-cost (both in money and latency) it is important that duplication of data
sent over this link be minimised, hence the requirement for a central hub for
game information. The server also minimises the modelling requirements of the
game engine, allowing the game clients to be simply views on the model.

As I have previous experience in both modelling and writing servers in Python
this task fell to me.

As well as writing the server I assisted other members of the team in using
Python and ZeroMQ; including writing some debugging code for the serial
connection. I also wrote the code that our website runs on.

### System requirements
Following are the formal requirements of the server system:

* Accept location input from nodes and update the *current* state of the world
  to match this.
* Similarly keep an up to date representation of the topography of the game
  area from node inputs.
* Accept and forward commands for nodes from the game clients.
* Inform game clients of the current state of the world to the point that they
  are allowed to know it (this means not telling them the location of the
  opposing team's players)
* Due to the slow pace of the game (human walking speed being rather low
  compared to the size of the game area) latency (and therefore server
  performance) is not not a focus.

## Design decisions
As the server would be communicating via TCP/IP with the other components I had
free choice of programming language. I chose to use Python simply because I am
an experienced Python programmer and it has the appropriate libraries.

For networking we decided to use a communication library that would abstract
away from raw sockets. We chose ZeroMQ for this purpose as it is well regarded
and multilingual, although our network code ended up all being written in
Python anyway. We also chose to use JSON as our network interchange format for
all communications above the base station in the stack as it has low overhead
and good library support. In particular there are methods in ZeroMQ dedicated
to JSON communication.

## Methodology
Following standard practices, the team used version control to assist in
collaboration. We settled upon git as our version control system as many
members of the team were already comfortable with it.

I perfomed no unit testing as I am unfamiliar with the practice, though I
appreciate its importance and acknowledge that it is a gap in both my knowledge
and the robustness of the server system.

I initially wrote the world model code as it was the core of the system and the
communication of this information to other parts of the system consisted of
views upon and controllers of this model.

Following the model I began experimenting with ZeroMQ to acquire an
understanding of the library. After some time settling on the use of ZeroMQ's
built-in ioloop implementation and JSON sending/receiving methods I set about
adding a server to the model.

In the process of doing so I wrote a simple telnet-like client for ZeroMQ that
allowed me to send strings to the server from a connecting client so that I
could test that it worked. As the server took shape I adapted this client to
provide some predefined strings to be sent that were designed to test each
send/receive component of the server.

## Network handshake
The server was designed to allow for many teams to connect along with the base
station. To facilitate this the server listens initially on a single port which
then provides new sockets and ports to clients that connect and make correct
requests.

Once a client has received its new port it communicates solely on that port.
This makes it easy to distinguish requests without having to include client
identifiers in requests.

## Model components
### World
Contains the other objects and tracks the topography mesh and step data.

## Analysis
* Calculate network speeds, latency?
